import sys
import ROOT
from ROOT import TH1F, TTree
import xgboost as xgb
from xgboost import plot_importance
from matplotlib import pyplot as plt
import numpy as np
import root_numpy

# accessing the data from ntuples
filename = sys.argv[1]
file = ROOT.TFile(filename)
tree_s = file.Get("t_s")
tree_b = file.Get("t_b")

n_sig = tree_s.GetEntries()
n_bkg = tree_b.GetEntries()

i = 0
N = 4
signal = np.array(np.arange(N))
background = np.array(np.arange(N))
tt = np.array(np.arange(N))
VV = np.array(np.arange(N))
ttV = np.array(np.arange(N))
higgs = np.array(np.arange(N))

event = []
weight_s = []
weight_b = []
weight_tt = []
weight_VV = []
weight_ttV = []
weight_higgs = []
label = []

# filling the training dataset
for branch in tree_s:
    event.clear()
    
    event.append(branch.ttBDT)
    event.append(branch.VVBDT)
    event.append(branch.ttVBDT)
    event.append(branch.higgsBDT)
    signal = np.row_stack((signal,event))
    weight_s.append(branch.mcWeight)
    label.append(1)

    i+=1
    
    print("Processing:{0}%".format(round((i + 1)*100/n_sig)), end="\r")

signal = np.delete(signal,-len(signal),axis=0)
print("Signal loading finish, entries:%d"%n_sig)

i = 0
for branch in tree_b:
    event.clear()

    event.append(branch.ttBDT)
    event.append(branch.VVBDT)
    event.append(branch.ttVBDT)
    event.append(branch.higgsBDT)
    background = np.row_stack((background,event))
    weight_b.append(branch.mcWeight)
    label.append(0)
    if branch.bkg == 0:
        tt = np.row_stack((tt,event))
        weight_tt.append(branch.mcWeight)
    elif branch.bkg == 1:
        VV = np.row_stack((VV,event))
        weight_VV.append(branch.mcWeight)
    elif branch.bkg == 2:
        ttV = np.row_stack((ttV,event))
        weight_ttV.append(branch.mcWeight)
    elif branch.bkg == 3:
        higgs = np.row_stack((higgs,event))
        weight_higgs.append(branch.mcWeight)

    i+=1
    print("Processing:{0}%".format(round((i + 1)*100/n_bkg)), end="\r")

background = np.delete(background,-len(background),axis=0)
tt = np.delete(tt,-len(tt),axis=0)
VV = np.delete(VV,-len(VV),axis=0)
ttV = np.delete(ttV,-len(ttV),axis=0)
higgs = np.delete(higgs,-len(higgs),axis=0)
print("Background loading finish, entries:%d"%n_bkg)

weight_s = np.array(weight_s)
weight_b = np.array(weight_b)
weight_tt = np.array(weight_tt)
weight_VV = np.array(weight_VV)
weight_ttV = np.array(weight_ttV)
weight_higgs = np.array(weight_higgs)
label = np.array(label)
# feature = np.array(feature)

params = {
    'booster': 'gbtree',
    # 'objective': 'multi:softmax',
    'objective': 'binary:logitraw',
    # 'num_class': 3,
    'gamma': 0.1,
    'max_depth': 3,
    'lambda': 2,
    'subsample': 0.7,
    'colsample_bytree': 0.7,
    'min_child_weight': 3,
    'eta': 0.1,
    'seed': 1000,
    'nthread': 16,
}

# training and saving models
dtrain = xgb.DMatrix(np.row_stack((signal,background)), label = label, weight = np.append(100*weight_s,100*weight_b))
num_rounds = 500
model = xgb.train(params, dtrain, num_rounds)
modelname = "output"
model.save_model('Model/'+modelname+".model")
model.dump_model('Model/'+modelname+'dump.txt')

# model application
# modelname = ["tt","VV","ttV","higgs"]
nbin = 7

hist_tt = TH1F("BDT","BDT",nbin,1,8)
hist_VV = TH1F("BDT","BDT",nbin,1,8)
hist_ttV = TH1F("BDT","BDT",nbin,1,8)
hist_higgs = TH1F("BDT","BDT",nbin,1,8)
hist_sig = TH1F("BDT","BDT",nbin,1,8)
hist_data = TH1F("BDT","BDT",nbin,1,8)

sig = xgb.DMatrix(signal)
bdt_sig = model.predict(sig)
tt = xgb.DMatrix(tt)
bdt_tt = model.predict(tt)
VV = xgb.DMatrix(VV)
bdt_VV = model.predict(VV)
ttV = xgb.DMatrix(ttV)
bdt_ttV = model.predict(ttV)
higgs = xgb.DMatrix(higgs)
bdt_higgs = model.predict(higgs)

root_numpy.fill_hist(hist_sig,-bdt_sig,weight_s)
root_numpy.fill_hist(hist_tt,-bdt_tt,weight_tt)
root_numpy.fill_hist(hist_VV,-bdt_VV,weight_VV)
root_numpy.fill_hist(hist_ttV,-bdt_ttV,weight_ttV)
root_numpy.fill_hist(hist_higgs,-bdt_higgs,weight_higgs)

output = ROOT.TFile("Results/Final/Signal.root","recreate")
hist_sig.Write()
output = ROOT.TFile("Results/Final/ttbar.root","recreate")
hist_tt.Write()
output = ROOT.TFile("Results/Final/VV.root","recreate")
hist_VV.Write()
output = ROOT.TFile("Results/Final/ttV.root","recreate")
hist_ttV.Write()
output = ROOT.TFile("Results/Final/Single_H.root","recreate")
hist_higgs.Write()

output.Close()