##==================
##ttH (mH = 125 GeV)
##==================
##XS =  507.1 fb
##QCD_scale      = +5.8 -9.2 %
##PDF (+alpha_S) = +- 3.6 %
## This and other Higgs production in:
## https://twiki.cern.ch/twiki/bin/view/LHCPhysics/CERNYellowReportPageAt13TeV#ttH_Process
XXX_ttH_XS_QCDscale_UP:     0.058
XXX_ttH_XS_QCDscale_DO: -0.092
XXX_ttH_XS_PDFunc_UP:  0.036
XXX_ttH_XS_PDFunc_DO: -0.036
#
##==================
##tH t-channel (mH = 125 GeV)
##==================
##XS = 7.425E-02 pb
##QCD_scale      = +6.5 -14.9 %
##PDF (+alpha_S) = +- 3.7 %
#XXX_tHjb_XS_QCDscale_UP:  0.065
#XXX_tHjb_XS_QCDscale_DO: -0.149 updated!!!
XXX_tHjb_XS_QCDscale_UP:  0.065
XXX_tHjb_XS_QCDscale_DO: -0.147
XXX_tHjb_XS_PDFunc_UP:    0.037
XXX_tHjb_XS_PDFunc_DO: -0.037
#
##==================
##WtH (mH = 125 GeV) - preliminary
##==================
##XS = 1.517E-02 pb
##QCD_scale      = +6.5 -6.7 %
##PDF (+alpha_S) = +- 6.3 %
#XXX_WtH_XS_QCDscale_UP:  0.065 updated!!!
#XXX_WtH_XS_QCDscale_DO: -0.067
XXX_WtH_XS_QCDscale_UP:  0.049
XXX_WtH_XS_QCDscale_DO: -0.067
XXX_WtH_XS_PDFunc_UP:    0.063
XXX_WtH_XS_PDFunc_DO: -0.063
#
##==================
##Branching ratio uncertaintes (YR4):
##==================
XXX_ATLAS_BR_bb_UP:     0.012
XXX_ATLAS_BR_bb_DO:  -0.013
XXX_ATLAS_BR_WW_UP:     0.016
XXX_ATLAS_BR_WW_DO:  -0.015
XXX_ATLAS_BR_ZZ_UP:     0.016
XXX_ATLAS_BR_ZZ_DO:  -0.015
XXX_ATLAS_BR_tautau_UP:     0.017
XXX_ATLAS_BR_tautau_DO:  -0.017
XXX_ATLAS_BR_gg_UP:     0.051
XXX_ATLAS_BR_gg_DO:  -0.051
XXX_ATLAS_BR_gamgam_UP:     0.021
XXX_ATLAS_BR_gamgam_DO:  -0.021
XXX_ATLAS_BR_Others_UP:     0.05
XXX_ATLAS_BR_Others_DO:  -0.05
#
##==================
##ttV uncertaintes (YR4):
##==================
## XSEC NLO using fixed scale (mt + mV/2)
## ttZ on-shell XSEC = 839.3 fb
## k-factor for ttll (on+off shell) = 1.51
## k-factor for ttZnnqq = 1.47
## ttW XSEC = 600.8 fb
## k-factor for ttW = 1.32
## https://twiki.cern.ch/twiki/bin/view/LHCPhysics/LHCHXSWGTTH#Plans_for_YR4
XXX_ttZ_XS_QCDscale_UP:     0.096
XXX_ttZ_XS_QCDscale_DO: -0.113
XXX_ttZ_XS_PDFunc_UP:  0.04
XXX_ttZ_XS_PDFunc_DO: -0.04
XXX_ttW_XS_QCDscale_UP:     0.129
XXX_ttW_XS_QCDscale_DO: -0.115
XXX_ttW_XS_PDFunc_UP:  0.034
XXX_ttW_XS_PDFunc_DO: -0.034
#
##==================
##tZ uncertaintes (preliminary):
##==================
XXX_tZ_XS_UP:  0.50
XXX_tZ_XS_DO: -0.50
##==================
##WtZ uncertaintes (preliminary):
##==================
XXX_WtZ_XS_UP:  0.50
XXX_WtZ_XS_DO: -0.50
#
##==================
##Dibosons uncertaintes:
##==================
XXX_diboson_XS_UP:  0.50
XXX_diboson_XS_DO: -0.50
##==================
##ttbar uncertaintes:
##==================
XXX_ttbar_XS_UP:  0.06
XXX_ttbar_XS_DO: -0.06
##==================
##Single top (t-channel, s-channel, Wt) uncertaintes:
##==================
#XXX_singletop_XS_UP:  0.05 5% for tW only. updated!!!
#XXX_singletop_XS_DO: -0.05
XXX_singletop_XS_UP:  0.04
XXX_singletop_XS_DO: -0.04
##==================
##V+jets uncertaintes:
##==================
##XXX_Zjets_XS_UP:  0.05
##XXX_Zjets_XS_DO: -0.05
##XXX_Wjets_XS_UP:  0.05
##XXX_Wjets_XS_DO: -0.05
XXX_Zjets_SherpaVar_UP:  0.05
XXX_Zjets_SherpaVar_DO: -0.05
XXX_Zjets_XS_UP:  0.40
XXX_Zjets_XS_DO: -0.40
XXX_Wjets_XS_UP:  0.40
XXX_Wjets_XS_DO: -0.40
XXX_Zjets_XS_2b_UP:  0.40
XXX_Zjets_XS_2b_DO: -0.40
XXX_Zjets_XS_3b_UP:  0.40
XXX_Zjets_XS_3b_DO: -0.40
XXX_Zjets_XS_4b_UP:  0.40
XXX_Zjets_XS_4b_DO: -0.40
XXX_Wjets_XS_2b_UP:  0.40
XXX_Wjets_XS_2b_DO: -0.40
XXX_Wjets_XS_3b_UP:  0.40
XXX_Wjets_XS_3b_DO: -0.40
XXX_Wjets_XS_4b_UP:  0.40
XXX_Wjets_XS_4b_DO: -0.40
##==================
## 4top uncertaintes:
##==================
##https://twiki.cern.ch/twiki/bin/view/AtlasProtected/CrossSectionNLOttV
##https://arxiv.org/pdf/1405.0301v2.pdf
##XS = 9.201 fb
XXX_tttt_XS_scale_UP:  0.308
XXX_tttt_XS_scale_DO: -0.256
XXX_tttt_XS_PDFunc_UP:    0.055
XXX_tttt_XS_PDFunc_DO: -0.059
## Overall unc to account for acceptance from bb
XXX_tttt_XS_UP:  0.50
XXX_tttt_XS_DO: -0.50
##==================
## ttWW (4f) uncertaintes:
##==================
##https://twiki.cern.ch/twiki/bin/view/AtlasProtected/CrossSectionNLOttV
##https://arxiv.org/pdf/1405.0301v2.pdf
##XS = 9.904 fb
XXX_ttWW_XS_scale_UP:  0.109
XXX_ttWW_XS_scale_DO: -0.118
XXX_ttWW_XS_PDFunc_UP:    0.021
XXX_ttWW_XS_PDFunc_DO: -0.021
##==================
## tZjb (t-channel 4f) uncertaintes:
##==================
##https://arxiv.org/pdf/1405.0301v2.pdf
##XS = 0.5657 pb
XXX_tZjb_XS_scale_UP:  0.077
XXX_tZjb_XS_scale_DO: -0.079
XXX_tZjb_XS_PDFunc_UP:    0.009
XXX_tZjb_XS_PDFunc_DO: -0.009
##==================
## Raw trigger matching selection is1L2Tau
##==================
##XXX_TRIGGER_SELECTION: (matchDLTll01||matchDLTll02||matchDLTll12||matchDLTll03||matchDLTll13||matchDLTll23)|| ( is1L2Tau&&(lep_isTrigMatch_0||lep_isTrigMatch_1||lep_isTrigMatch_2||lep_isTrigMatch_3) )
##XXX_DLTTRIGGER_SELECTION: (matchDLTll01||matchDLTll02||matchDLTll12||matchDLTll03||matchDLTll13||matchDLTll23)
XXX_TRIGGER_SELECTION: 1
XXX_DLTTRIGGER_SELECTION: 1
##==================
## Prompt selection when data-driven backgrounds are used
##==================
## require leptons to be prompt and not QmisID for 2lss0tau channel
## require (all??) leptons to be prompt for 3l channel
XXX_PROMPT_SELECTION: (dilep_type)
XXX_TIGHT_SELECTION:  (dilep_type)
#XXX_PROMPT_MCWEIGHT: (36074.6*(RunYear==2015||RunYear==2016)+43813.7*(RunYear==2017))*(1/79888.3)*scale_nom*pileupEventWeight_090*JVT_EventWeight*MV2c10_70_EventWeight
XXX_PROMPT_MCWEIGHT: (36000.0*(RunYear==2015||RunYear==2016)+104000*(RunYear==2017))*(1/140000.0)*mcWeightOrg*scale_nom*pileupEventWeight_090*JVT_EventWeight*MV2c10_70_EventWeight
XXX_LLSSNTAU_BASIC_SELECTION: dilep_type&&(lep_isolationFixedCutLoose_0&&lep_isolationFixedCutLoose_1)&&nJets_OR_T>=1
XXX_LLLNTAU_BASIC_SELECTION: dilep_type 
##==================
## For Fakes and QMisId exclusion in Instrumental systematics
##==================
XXX_EXCLUDE_DATA_DRIVEN: QMisId,QMisId_ee,QMisId_em,QMisId_me,QMisId_l20tau,QMisId_L2SS1Tau,Fakes,Fakes_l20tau,Fakes_l20tau_ee,Fakes_l20tau_em,Fakes_l20tau_me,Fakes_l20tau_mm,Fakes_l30tau,Fakes_L2OS1Tau,FakeTau_L31Tau,Fakes_L2SS1Tau,Fakes_L12Tau,FakeTau_L31Tau_jet, Fakes_l21tau_MM*
XXX_EXCLUDE_INSTRUMENTAL_NTUPLES: QMisId,QMisId_ee,QMisId_em,QMisId_me,QMisId_l20tau,QMisId_L2SS1Tau,Fakes,Fakes_l20tau,Fakes_l20tau_ee,Fakes_l20tau_em,Fakes_l20tau_me,Fakes_l20tau_mm,Fakes_l30tau,Fakes_L2OS1Tau,FakeTau_L31Tau,Fakes_L2SS1Tau,Fakes_L12Tau,FakeTau_L31Tau_jet,ttH_FF
##=================
## exclude from default samples
##=================
XXX_EXCLUDE_DEFAULT: L31Tau
##=================
## lumi for the MC subtractions
##=================
XXX_LUMI: 140000.0
##=================
## exclude samples with 0 yield for fit
##=================
XXX_EXCLUDE_TTW: l4_enrZ,l4_depZ
XXX_EXCLUDE_TTLL_LOWMASS: l4_enrZ,l4_depZ,HjLmZdep
XXX_EXCLUDE_DIBOSON: l4_depZ,l30tau_ttW
XXX_EXCLUDE_SINGLETOP: l20tau,l20tau_free,l30tau_ttH,l30tau_ttW,l30tau_ttZ,l30tau_VV,l30tau_ttbar_Rest,l4_enrZ,l4_depZ,L12Tau_BDT,L12Tau_SR,L12Tau_CR,L2SS1Tau_BDT,L2OS1Tau_BDT,L2OS1Tau_BDT_blind,L31Tau
XXX_EXCLUDE_VGAMMA: l20tau,l20tau_free,l30tau_ttH,l30tau_ttW,l30tau_ttZ,l30tau_VV,l30tau_ttbar_Rest,l4_enrZ,l4_depZ,L12Tau_BDT,L12Tau_SR,L12Tau_CR,L2SS1Tau_BDT,L31Tau
#XXX_EXCLUDE_TZ: l20tau,l20tau_free,l4_enrZ,l4_depZ
XXX_EXCLUDE_TW: l20tau,l20tau_free,l30tau_ttH,l30tau_ttW,l30tau_ttZ,l30tau_VV,l30tau_ttbar_Rest,l4_enrZ,l4_depZ,L12Tau_BDT,L2SS1Tau_BDT
XXX_EXCLUDE_WTZ: l4_depZ
XXX_EXCLUDE_RARETOP: l4_enrZ,l4_depZ,L12Tau_BDT,L12Tau_SR,L12Tau_CR
XXX_EXCLUDE_VVV: l4_depZ
XXX_EXCLUDE_VH: l30tau_ttH,l30tau_ttW,l30tau_ttZ,l30tau_VV,l30tau_ttbar_Rest,l4_enrZ,l4_depZ,L12Tau_BDT,L12Tau_SR,L12Tau_CR,L2SS1Tau_BDT,L2OS1Tau_BDT,L31Tau
XXX_EXCLUDE_THJB_WW: l4_enrZ,l4_depZ
XXX_EXCLUDE_THJB_ZZ: l4_enrZ,l4_depZ,L12Tau_BDT,L12Tau_SR,L12Tau_CR
XXX_EXCLUDE_THJB_TAUTAU: l4_enrZ,l4_depZ
XXX_EXCLUDE_THJB_OTHERS: l4_enrZ,l4_depZ
XXX_EXCLUDE_WTH_ZZ: l4_enrZ,l4_depZ,L12Tau_BDT,L12Tau_SR,L12Tau_CR
XXX_EXCLUDE_WTH_WW: L12Tau_BDT,L12Tau_SR,L12Tau_CR
XXX_EXCLUDE_WTH_TAUTAU: l4_depZ
XXX_EXCLUDE_WTH_OTHERS: l20tau,l20tau_free,l30tau_ttH,l30tau_ttW,l30tau_ttZ,l30tau_VV,l30tau_ttbar_Rest,l4_enrZ,l4_depZ,L2SS1Tau_BDT,L12Tau_BDT
XXX_EXCLUDE_TTH_ZZ: LjLm1bZdep,HjHmZdep
##=================
## 2lss and 3l regions to be considered for fakes
##=================
XXX_LLSS_FAKES_REGIONS: l20tau,l20tau_free,l20tau_1b_mm,l20tau_1b_em,l20tau_1b_ee,l20tau_2b_mm,l20tau_2b_em,l20tau_2b_ee,l20tau_1b_mm_4j,l20tau_1b_em_4j,l20tau_1b_ee_4j,l20tau_2b_mm_4j,l20tau_2b_em_4j,l20tau_2b_ee_4j,l20tau_first4bins,l20tau_free_first4bins,l20tau_pt20,l20tau_10bins,l20tau_pt20_first4bins,l20tau_pt20_10bins,l20tau_pt20_free,l20tau_pt20_free_first4bins,l20tau_pt20_3b,l20tau_3b,l20tau_pt20_3b_mm,l20tau_3b,l20tau_3b_mm,l20tau_lep_Pt1,l20tau_lep_Pt0,l20tau_lep_eta0,l20tau_lep_eta1,l20tau_nJets,l20tau_nBjets70,l20tau_MET,l20tau_dRl0j,l20tau_dRl0jmu,l20tau_dRl0jel,l20tau_dRl1j,l20tau_dRl1jmu,l20tau_dRl1jel,l20tau_lFlav,l20tau_maxEta,l20tau_Mll,l20tau_DRll,l20tau_BDTGttV10bins,l20tau_BDTGttbar9bins,l20tau_BDTG10bins,l20tau_*
XXX_LLSS_FAKES_PT15_REGIONS: l20tau_pt15,l20tau_pt15_free,l20tau_pt15_first4bins,l20tau_pt15_free_first4bins,l20tau_pt15_10bins
XXX_LLL_FAKES_REGIONS: l30tau_ttH,l30tau_ttW,l30tau_ttbar_Rest,l30tau_ttZ,l30tau_VV,l30tau_depZ,l30tau_enrZ,LjLm1bZdep,LjLm2bZdep,LjLm1bZenr,LjLm2bZenr,LjZpeak,LjHmZdep,LjHmZenr,HjLmZdep,HjLmZenr,HjZpeak,HjHmZdep,HjHmZenr,l30tau_depZ_7_0,l30tau_enrZ_0_3,l30tau_enrZ_2bins,l30tau_depZ_first4bins,l30tau_ttH_firstBin
##=================
## 2LOS fit and validation regions
##=================
XXX_L2OS1TAU_REGIONS: L2OS1Tau,L2OS1Tau_BDT,L2OS1Tau_BDT_blind,L2OS1Tau_NJet,L2OS1Tau_NBJet,L2OS1Tau_SumPtJet,L2OS1Tau_SumPtBJet,L2OS1Tau_SumPtLep,L2OS1Tau_Jet0Pt,L2OS1Tau_Mll01,L2OS1Tau_DRl0tau,L2OS1Tau_DRl1tau,L2OS1Tau_DRlj,L2OS1Tau_DRlb,L2OS1Tau_DRjb,L2OS1Tau_Tau0Pt
##=================
## 2LSS1Tau fit and validation regions
##=================
XXX_L2SS1Tau_REGIONS: L2SS1Tau_BDT,L2SS1Tau_CAT1,L2SS1Tau_CAT2,L2SS1Tau_CAT3,L2SS1Tau_NJet,L2SS1Tau_NBJet,L2SS1Tau_Lep1Pt,L2SS1Tau_MaxEta,L2SS1Tau_Jet3Eta,L2SS1Tau_MVisNonH,L2SS1Tau_SubLeadJetPt,L2SS1Tau_HT_jets,L2SS1Tau_Jet1PCB,L2SS1Tau_Jet2PCB,L2SS1Tau_DRLeadLepCloseJet,L2SS1Tau_Lep1PtVarCone,L2SS1Tau_DRLeadJets,L2SS1Tau_Lep0Pt,L2SS1Tau_Lep0Eta,L2SS1Tau_Lep1Eta,L2SS1Tau_Tau0Pt,L2SS1Tau_TauNTrack,L2SS1Tau_LeadJetPt
##=================
## smoothing of data-driven weight systematics
##=================
XXX_SMOOTHING_DATA_DRIVEN: 40
XXX_SMOOTHING_WEIGHTS: 40
XXX_SMOOTHING_TREES: 40
XXX_SMOOTHING_SHAPES: 40
XXX_SMOOTHING_SCALES: 40
##=================
## floating ttV normalisation, set to 'all' or 'none' for fixed or free floating
##=================
XXX_REGIONS_FLOAT_TTW: none
XXX_REGIONS_FLOAT_TTZ: none
XXX_REGIONS_NOT_FLOAT_TTW: all
XXX_REGIONS_NOT_FLOAT_TTZ: all
XXX_REGIONS_FLOAT_TTV: none
##=================
## values to change for unblinded data fit with fixed muttH=1
##=================
XXX_MU_XS_TTH_MIN: -10.
XXX_MU_XS_TTH_MAX: 20.
XXX_FIT_BLIND: TRUE
XXX_REGIONS_FLOAT_TTH: all
XXX_BLINDING_THRESHOLD: 0.15
##=================
## values to change for unblinded data fit without ttH_XS NPs (for XS measurement)
##=================
XXX_REGIONS_XS_TTH: all
##=================
## options for 7mu
##=================
XXX_POI: mu_XS_ttH
XXX_CONSTANT_MULTIMU: TRUE
XXX_CONSTANT_SINGLEMU: FALSE
##=================
## labelsssss
##=================
XXX_ATLAS_LABEL: Internal
XXX_AVAILABLE_SYS: ttW,ttZ,ttll_lowMass,rareTop,threeTop,fourTop,ttWW,tZ,VVV,VH,tW,ttH
##. WtZ missing
##=================
## Added by Xuan for 2lss1tau
##=================
XXX_ADDITIONAL_CUT: ((abs(lep_ID_0)==11&&abs(lep_Eta_0)<2.0||abs(lep_ID_0)==13)&&(abs(lep_ID_1)==11&&abs(lep_Eta_1)<2.0||abs(lep_ID_1)==13)&&Mll01>12e3&&(lep_flavour==3||DRll01>0.5))
XXX_TIGHT_LEPTON: (((abs(lep_ID_0)==13 && lep_isolationFixedCutLoose_0 && lep_isMedium_0 && lep_promptLeptonVeto_TagWeight_0<-0.5) || (abs(lep_ID_0)==11 && lep_isolationFixedCutLoose_0 && lep_isTightLH_0 && lep_chargeIDBDTTight_0>0.7 && lep_ambiguityType_0 == 0 && lep_promptLeptonVeto_TagWeight_0<-0.7)) &&((abs(lep_ID_1)==13 && lep_isolationFixedCutLoose_1 && lep_isMedium_1 && lep_promptLeptonVeto_TagWeight_1<-0.5) || (abs(lep_ID_1)==11 && lep_isolationFixedCutLoose_1 && lep_isTightLH_1 && lep_chargeIDBDTTight_1>0.7 && lep_ambiguityType_1 == 0 && lep_promptLeptonVeto_TagWeight_1<-0.7)))
XXX_LEPTON_PT: (lep_Pt_0>20e3&&lep_Pt_1>20e3)
XXX_L2SS1Tau_SideBand: (dilep_type&&nTaus_OR_Pt25==1&&tau_tagWeightBin_0<=3&&tau_fromPV_0==1&&tau_passMuonOLR_0 == 1&&tau_charge_0*lep_ID_0>0&&nJets_OR_T>=4&&nJets_OR_T_MV2c10_70&&(abs(Mll01-91.2e3)>10e3||abs(lep_ID_0)!=11||abs(lep_ID_1)!=11))
XXX_L2SS1Tau_LowNj_SideBand: (dilep_type&&nTaus_OR_Pt25==1&&tau_tagWeightBin_0<=3&&tau_fromPV_0==1&&tau_passMuonOLR_0 == 1&&tau_charge_0*lep_ID_0>0&&nJets_OR_T>=2&&nJets_OR_T<=3&&nJets_OR_T_MV2c10_70&&(abs(Mll01-91.2e3)>10e3||abs(lep_ID_0)!=11||abs(lep_ID_1)!=11))
XXX_SS2L_SELECTION: (lep_ID_0*lep_ID_1>0)
##=================
## from 3l1tau
##=================
XXX_PROMPT_MCLOOSEWEIGHT: (36074.6*(RunYear==2015||RunYear==2016)+43813.7*(RunYear==2017))*(1/79888.3)*scale_nom*pileupEventWeight_090*JVT_EventWeight*MV2c10_70_EventWeight*SherpaNJetWeight*(lepSFObjLoose*lepSFTrigLoose*isMMSideband_TT+isMMSideband_TAntiT*lepSFTrigTightLoose*lep_SFObjTight_0*lep_SFObjLoose_1+isMMSideband_AntiTT*lepSFTrigLooseTight*lep_SFObjLoose_0**lep_SFObjTight_1+isMMSideband_AntiTAntiT*lepSFObjLoose*lepSFTrigLoose)
XXX_TP_PROMPT_MCWEIGHT: (36074.6*(RunYear==2015||RunYear==2016)+43813.7*(RunYear==2017))*(1/79888.3)*scale_nom*pileupEventWeight_090*JVT_EventWeight*MV2c10_70_EventWeight
SF_LEPONE: (abs(lep_ID_0)==13&&lep_isolationFixedCutLoose_0&&lep_promptLeptonVeto_TagWeight_0<-0.5)||(abs(lep_ID_0)==11&&lep_isolationFixedCutLoose_0&&lep_isTightLH_0&&lep_chargeIDBDTTight_0>0.7&&lep_ambiguityType_0==0&&lep_promptLeptonVeto_TagWeight_0<-0.7)
SF_LEPTWO: (abs(lep_ID_1)==13&&lep_isolationFixedCutLoose_1&&lep_promptLeptonVeto_TagWeight_1<-0.5)||(abs(lep_ID_1)==11&&lep_isolationFixedCutLoose_1&&lep_isTightLH_1&&lep_chargeIDBDTTight_1>0.7&&lep_ambiguityType_1==0&&lep_promptLeptonVeto_TagWeight_1<-0.7)
SF_LEPTHREE: (abs(lep_ID_2)==13&&lep_isolationFixedCutLoose_2&&lep_promptLeptonVeto_TagWeight_2<-0.5)||(abs(lep_ID_2)==11&&lep_isolationFixedCutLoose_2&&lep_isTightLH_2&&lep_chargeIDBDTTight_2>0.7&&lep_ambiguityType_2==0&&lep_promptLeptonVeto_TagWeight_2<-0.7)
#
ELECTRONONETIGHT: (abs(lep_ID_0)==11 && lep_isolationFixedCutLoose_0 && lep_isTightLH_0 && lep_chargeIDBDTTight_0>0.7 && lep_ambiguityType_0==0 && lep_promptLeptonVeto_TagWeight_0<-0.7)
ELECTRONTWOTIGHT: (abs(lep_ID_1)==11 && lep_isolationFixedCutLoose_1 && lep_isTightLH_1 && lep_chargeIDBDTTight_1>0.7 && lep_ambiguityType_1==0 && lep_promptLeptonVeto_TagWeight_1<-0.7)
ELECTRONTHREETIGHT: (abs(lep_ID_2)==11 && lep_isolationFixedCutLoose_2 && lep_isTightLH_2 && lep_chargeIDBDTTight_2>0.7 && lep_ambiguityType_2==0 && lep_promptLeptonVeto_TagWeight_2<-0.7)
TAGLEPTONONE: ((lep_isPrompt_0||(abs(lep_ID_0)==13&&lep_truthOrigin_0==0)||(abs(lep_ID_0)==11&&lep_truthOrigin_0==5&&lep_truthParentPdgId_0==lep_ID_0&&lep_truthParentType_0==2))&&!(lep_isQMisID_0==1)&&((abs(lep_ID_0)==13 && lep_isolationFixedCutLoose_0 && lep_promptLeptonVeto_TagWeight_0<-0.5) || (abs(lep_ID_0)==11 && lep_isolationFixedCutLoose_0 && lep_isTightLH_0 && lep_chargeIDBDTTight_0>0.7 && lep_ambiguityType_0==0 && lep_promptLeptonVeto_TagWeight_0<-0.7)))
TAGLEPTONTWO: ((lep_isPrompt_1||(abs(lep_ID_1)==13&&lep_truthOrigin_1==0)||(abs(lep_ID_1)==11&&lep_truthOrigin_1==5&&lep_truthParentPdgId_1==lep_ID_1&&lep_truthParentType_1==2))&&!(lep_isQMisID_1==1)&&((abs(lep_ID_1)==13 && lep_isolationFixedCutLoose_1 && lep_promptLeptonVeto_TagWeight_1<-0.5) || (abs(lep_ID_1)==11 && lep_isolationFixedCutLoose_1 && lep_isTightLH_1 && lep_chargeIDBDTTight_1>0.7 && lep_ambiguityType_1==0 && lep_promptLeptonVeto_TagWeight_1<-0.7)))
TAGLEPTONTHREE: ((lep_isPrompt_2||(abs(lep_ID_2)==13&&lep_truthOrigin_2==0)||(abs(lep_ID_2)==11&&lep_truthOrigin_2==5&&lep_truthParentPdgId_2==lep_ID_2&&lep_truthParentType_2==2))&&!(lep_isQMisID_2==1)&&((abs(lep_ID_2)==13 && lep_isolationFixedCutLoose_2 && lep_promptLeptonVeto_TagWeight_2<-0.5) || (abs(lep_ID_2)==11 && lep_isolationFixedCutLoose_2 && lep_isTightLH_2 && lep_chargeIDBDTTight_2>0.7 && lep_ambiguityType_2==0 && lep_promptLeptonVeto_TagWeight_2<-0.7)))
MUON_MEDIUM: (((abs( lep_ID_0 ) == 11 ||( abs( lep_ID_0 ) == 13&&lep_isMedium_0)) && ((abs( lep_ID_1 ) == 13&&lep_isMedium_1)||(abs( lep_ID_1 ) == 11))) && ((abs( lep_ID_2 ) == 13&&lep_isMedium_2)||(abs( lep_ID_2 ) == 11)))
