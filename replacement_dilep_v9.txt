#######LeptonIDCuts######
DIF_LHXXX: (lep_isLooseLH_0  && lep_isLoose_1) || (lep_isLoose_0  && lep_isLooseLH_1)
SMF_LHXXX: (lep_isLooseLH_0  && lep_isLooseLH_1) || (lep_isLoose_0  && lep_isLoose_1)
EE_LHXXX: lep_isLooseLH_0  && lep_isLooseLH_1
MUMU_LHXXX: lep_isLoose_0  && lep_isLoose_1
EMU_LHXXX: lep_isLooseLH_0  && lep_isLoose_1
MUE_LHXXX: lep_isLoose_0  && lep_isLooseLH_1
INC_LHXXX_OLD: (lep_isLooseLH_0||lep_isLoose_0)  && (lep_isLooseLH_1||lep_isLoose_1) 
###########Loose###########
INC_LHXXX: ((lep_isLooseLH_0 == 1)||(lep_isLoose_0 == 1)) && ((lep_isLooseLH_1 == 1)||(lep_isLoose_1 == 1))
INC_LHXXX_TIGHT: ((lep_isTightLH_0 == 1)||(lep_isTight_0 == 1)) && ((lep_isTightLH_1 == 1)||(lep_isTight_1 == 1))
INC_LHXXX_MEDIUM: ((lep_isMediumLH_0 == 1)||(lep_isMedium_0 == 1)) && ((lep_isMediumLH_1 == 1)||(lep_isMedium_1 == 1))
##########
INC_ISOLLLXXX: ((lep_isolationFCLoose_0 == 1) || (lep_isolationPflowLoose_0 == 1)) && ((lep_isolationFCLoose_1 == 1) || (lep_isolationPflowLoose_1 == 1))
INC_ISOLLLXXX_TIGHT: ((lep_isolationFCTight_0 == 1) || (lep_isolationPflowTight_0 == 1)) && ((lep_isolationFCTight_1 == 1) || (lep_isolationPflowTight_1 == 1))
###############
TAUVETO: (nTaus_OR_Pt25_RNN == 0)
################
TRIGGER: (lep_isTrigMatch_0 || lep_isTrigMatchDLT_0) && (lep_isTrigMatch_1 || lep_isTrigMatchDLT_1)
############
CHIDBDT_XXX_OLD: (lep_chargeIDBDTTight_0 > -0.337671) &&  (lep_chargeIDBDTTight_1 > -0.337671)
CHIDBDT_XXX: ((lep_chargeIDBDTLoose_0 == 1) || (abs(lep_ID_0) == 13))  &&  ((lep_chargeIDBDTLoose_1 == 1) || (abs(lep_ID_1) == 13))
###########
OPPCHRG_XXX: (lep_ID_0*lep_ID_1 < 0)
TOTCHRG_XXX: (total_charge == 0)
##MC WEIGHT##
#XXX_PROMPT_MCWEIGHT: (36000.0*(RunYear==2015||RunYear==2016)+104000.0*(RunYear==2017))*(1.0/140000.0)*mcWeightOrg*scale_nom*pileupEventWeight_090*JVT_EventWeight*MV2c10_70_EventWeight
XXX_PROMPT_MCWEIGHT_V10: (36074.6*(RunYear==2015||RunYear==2016)+43813.7*(RunYear==2017)+58450.1*(RunYear==2018))*weight_pileup*weight_jvt*mc_xSection*mc_kFactor*weight_mc/totalEventsWeighted
