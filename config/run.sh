#!/bin/bash

count=0
cat cut.txt | while read line; 
do  
    count=$((count+1))
    cp dileptemp.config tmp.config
    perl -pi -e "s/line/$line/g" tmp.config
    trex-fitter nws  tmp.config  2>&1 > $count.log
    cat $count.log | grep "Median significance"
done
