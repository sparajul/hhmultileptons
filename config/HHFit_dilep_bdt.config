Job: "HH_ML_DILEP_BDTG"
  Label: "HH_ML_DILEP_BDTG"
  CmeLabel: "13 TeV"
  LumiLabel: "140 fb^{-1}"
  Lumi: 140000.0
  POI: "SigXsecOverSM"
  ReadFrom: NTUP
  PlotOptions: YIELDS,CHI2,NORMSIG
  #NtuplePaths:/data/rohin/HHML_ntuples/v3/
  NtuplePaths:/users/santoshp/scratch/hhml
  DebugLevel: 10
  SystControlPlots: TRUE
  ReplacementFile: replacement_dilep.txt
  ImageFormat: root


Fit: "myFit"
  FitType: SPLUSB
  FitRegion: CRSR

Region: l20tauOS_nJets
  Label:  $2\ell OS 0 \tau$
  Type: VALIDATION
  Variable: nJets_OR_T,10,-0.5,9.5
  VariableTitle: nJets
  LogScale: False
  Selection: (dilep_type) && (TOTCHRG_XXX) && (INC_LHXXX) && (CHIDBDT_XXX) && (nJets_OR_T_MV2c10_70 ==0)  && (nJets_OR_T >1)

#Region: l20tauOS_nBjet
#  Label: $2\ell OS 0 \tau$
#  Type: VALIDATION
#  Variable: nJets_OR_T_MV2c10_70,10,-0.5,9.5
#  VariableTitle: nBjets
#  LogScale: False
#  Selection: (dilep_type) && (TOTCHRG_XXX) && (INC_LHXXX) && (CHIDBDT_XXX) && (nJets_OR_T_MV2c10_70 ==0)  && (nJets_OR_T >1)

Region: l20tau_lep_Pt_0
  Label: $2\ell OS 0 \tau$
  Type: VALIDATION
  Variable: lep_Pt_0/1e3,100,0,500
  VariableTitle: "leading lepton pT"
  LogScale: False
  Selection: (dilep_type) && (TOTCHRG_XXX) && (INC_LHXXX) && (CHIDBDT_XXX) && (nJets_OR_T_MV2c10_70 ==0)  && (nJets_OR_T >1)

Region: l20tau_lep_Pt_1
  Label: $2\ell OS 0 \tau$
  Type: VALIDATION
  Variable: lep_Pt_1/1e3,100,0,500
  VariableTitle: "subleading lepton pT"
  LogScale: False
  Selection: (dilep_type) && (TOTCHRG_XXX) && (INC_LHXXX) && (CHIDBDT_XXX) && (nJets_OR_T_MV2c10_70 ==0)  && (nJets_OR_T >1)

Region: l20tau_HT_lep
  Label: $2\ell OS 0 \tau$
  Type: VALIDATION
  Variable: HT_lep/1e3,100,0,700
  VariableTitle: "lepton HT"
  LogScale: False
  Selection: (dilep_type) && (TOTCHRG_XXX) && (INC_LHXXX) && (CHIDBDT_XXX) && (nJets_OR_T_MV2c10_70 ==0)  && (nJets_OR_T >1)

Region: l20tau_HT_jets
  Label: $2\ell OS 0 \tau$
  Type: VALIDATION
  Variable: HT_jets/1e3,100,10,700
  VariableTitle: "jet HT"
  LogScale: False
  Selection: (dilep_type) && (TOTCHRG_XXX) && (INC_LHXXX) && (CHIDBDT_XXX) && (nJets_OR_T_MV2c10_70 ==0)  && (nJets_OR_T >1)

Region: l20tau_Mll01
  Label: $2\ell OS 0 \tau$
  Type: VALIDATION
  Variable: Mll01/1e3,100,0,500
  VariableTitle: "Invariant Mass of leptons"
  LogScale: False
  Selection: (dilep_type) && (TOTCHRG_XXX) && (INC_LHXXX) && (CHIDBDT_XXX) && (nJets_OR_T_MV2c10_70 ==0)  && (nJets_OR_T >1)

Region: l20tau_DRll01
  Label: $2\ell OS 0 \tau$
  Type: VALIDATION
  Variable: DRll01,100,0,6
  VariableTitle: "DR_ll01"
  LogScale: False
  Selection: (dilep_type) && (TOTCHRG_XXX) && (INC_LHXXX) && (CHIDBDT_XXX) && (nJets_OR_T_MV2c10_70 ==0)  && (nJets_OR_T >1)

  Region: l20tau_MET
  Label: $2\ell OS 0 \tau$
  Type: VALIDATION
  Variable: MET_RefFinal_et/1e3,100,0,700
  VariableTitle: "MET"
  LogScale: False
  Selection: (dilep_type) && (TOTCHRG_XXX) && (INC_LHXXX) && (CHIDBDT_XXX) && (nJets_OR_T_MV2c10_70 ==0)  && (nJets_OR_T >1)

  Region: l20tau_BDTG
  Label: $2\ell OS 0 \tau$
  Type: VALIDATION
  Variable: bdtg_2lOS,20,-1.0,1.0
  VariableTitle: "BDTG"
  LogScale: True
  Selection: (dilep_type) && (TOTCHRG_XXX) && (INC_LHXXX) && (CHIDBDT_XXX) && (nJets_OR_T_MV2c10_70 ==0)  && (nJets_OR_T >1)

###################################################################################################################################
###################################################################################################################################
Sample: HH
  Type: SIGNAL
  Title: HH
  TexTitle: $HH$
  FillColor: 4
  LineColor: 4
  NtupleName: dilep
  NtupleFiles: nominal/mc16a/450661,nominal/mc16a/450662,nominal/mc16a/450663,nominal/mc16d/450661,nominal/mc16d/450662,nominal/mc16d/450663
  MCweight: XXX_PROMPT_MCWEIGHT

Sample: VV
  Type: BACKGROUND
  Title: VV
  TexTitle: $VV$
  FillColor: 3
  LineColor: 1
  NtupleName: dilep
  NtupleFiles: nominal/mc16a/364250,nominal/mc16a/364253,nominal/mc16a/364254,nominal/mc16a/364255,nominal/mc16a/364283,nominal/mc16a/364284,nominal/mc16a/364285,nominal/mc16a/364286,nominal/mc16a/364287,nominal/mc16a/363355,nominal/mc16a/363356,nominal/mc16a/363357,nominal/mc16a/363358,nominal/mc16a/363359,nominal/mc16a/363360,nominal/mc16a/363489,nominal/mc16d/364250,nominal/mc16d/364253,nominal/mc16d/364254,nominal/mc16d/364255,nominal/mc16d/364283,nominal/mc16d/364284,nominal/mc16d/364285,nominal/mc16d/364286,nominal/mc16d/364287,nominal/mc16d/363355,nominal/mc16d/363356,nominal/mc16d/363357,nominal/mc16d/363358,nominal/mc16d/363359,nominal/mc16d/363360,nominal/mc16d/363489
  MCweight: XXX_PROMPT_MCWEIGHT 
  %Exclude:l4_depZ

Sample: Zjets
  Type: BACKGROUND
  Title: "#it{Z} + jets "
  TexTitle: $Z+\text{jets}$
  Group: V+jets
  FillColor: 900
  LineColor: 1
  NtupleName: dilep
  NtupleFiles: nominal/mc16a/364100,nominal/mc16a/364101,nominal/mc16a/364102,nominal/mc16a/364103,nominal/mc16a/364104,nominal/mc16a/364105,nominal/mc16a/364106,nominal/mc16a/364107,nominal/mc16a/364108,nominal/mc16a/364109,nominal/mc16a/364110,nominal/mc16a/364111,nominal/mc16a/364112,nominal/mc16a/364113,nominal/mc16a/364114,nominal/mc16a/364115,nominal/mc16a/364116,nominal/mc16a/364117,nominal/mc16a/364118,nominal/mc16a/364119,nominal/mc16a/364120,nominal/mc16a/364121,nominal/mc16a/364122,nominal/mc16a/364123,nominal/mc16a/364124,nominal/mc16a/364125,nominal/mc16a/364126,nominal/mc16a/364127,nominal/mc16a/364128,nominal/mc16a/364129,nominal/mc16a/364130,nominal/mc16a/364131,nominal/mc16a/364132,nominal/mc16a/364133,nominal/mc16a/364134,nominal/mc16a/364135,nominal/mc16a/364136,nominal/mc16a/364137,nominal/mc16a/364138,nominal/mc16a/364139,nominal/mc16a/364140,nominal/mc16a/364141,nominal/mc16d/364100,nominal/mc16d/364101,nominal/mc16d/364102,nominal/mc16d/364103,nominal/mc16d/364104,nominal/mc16d/364105,nominal/mc16d/364106,nominal/mc16d/364107,nominal/mc16d/364108,nominal/mc16d/364109,nominal/mc16d/364110,nominal/mc16d/364111,nominal/mc16d/364112,nominal/mc16d/364113,nominal/mc16d/364114,nominal/mc16d/364115,nominal/mc16d/364116,nominal/mc16d/364117,nominal/mc16d/364118,nominal/mc16d/364119,nominal/mc16d/364120,nominal/mc16d/364121,nominal/mc16d/364122,nominal/mc16d/364123,nominal/mc16d/364124,nominal/mc16d/364125,nominal/mc16d/364126,nominal/mc16d/364127,nominal/mc16d/364128,nominal/mc16d/364129,nominal/mc16d/364130,nominal/mc16d/364131,nominal/mc16d/364132,nominal/mc16d/364133,nominal/mc16d/364134,nominal/mc16d/364135,nominal/mc16d/364136,nominal/mc16d/364137,nominal/mc16d/364138,nominal/mc16d/364139,nominal/mc16d/364140,nominal/mc16d/364141
  MCweight: XXX_PROMPT_MCWEIGHT

Sample: ttZ
  Type: BACKGROUND
  Title: "#it{t#bar{t}}(#it{Z}/#it{#gamma}*)"
  TexTitle: $t\bar{t}Z/\gamma$
  Group: "#it{t#bar{t}}(#it{Z}/#it{#gamma}*)"
  FillColor: 7
  LineColor: 1
  NtupleName: dilep
  NtupleFiles:  nominal/mc16a/410156,nominal/mc16a/410157,nominal/mc16a/410218,nominal/mc16a/410219,nominal/mc16a/410220,nominal/mc16d/410156,nominal/mc16d/410157,nominal/mc16d/410218,nominal/mc16d/410219,nominal/mc16d/410220
  MCweight: XXX_PROMPT_MCWEIGHT

Sample: ttW
  Type: BACKGROUND
  Title: "#it{t#bar{t}W}"
  TexTitle: $t\bar{t}W$
  FillColor: 5
  LineColor: 1
  NtupleName: dilep
  NtupleFiles: nominal/mc16a/413008,nominal/mc16d/413008
  MCweight: XXX_PROMPT_MCWEIGHT

Sample: Wjets
  Type: BACKGROUND
  Title: "#it{W} + jets"
  TexTitle: $W+\text{jets}$
  Group: V+jets
  FillColor: 900
  LineColor: 1
  NtupleName: dilep
  NtupleFiles: nominal/mc16a/364156,nominal/mc16a/364157,nominal/mc16a/364158,nominal/mc16a/364159,nominal/mc16a/364160,nominal/mc16a/364161,nominal/mc16a/364162,nominal/mc16a/364163,nominal/mc16a/364164,nominal/mc16a/364165,nominal/mc16a/364166,nominal/mc16a/364167,nominal/mc16a/364168,nominal/mc16a/364169,nominal/mc16a/364170,nominal/mc16a/364171,nominal/mc16a/364172,nominal/mc16a/364173,nominal/mc16a/364174,nominal/mc16a/364175,nominal/mc16a/364176,nominal/mc16a/364177,nominal/mc16a/364178,nominal/mc16a/364179,nominal/mc16a/364180,nominal/mc16a/364181,nominal/mc16a/364182,nominal/mc16a/364183,nominal/mc16a/364184,nominal/mc16a/364185,nominal/mc16a/364186,nominal/mc16a/364187,nominal/mc16a/364188,nominal/mc16a/364189,nominal/mc16a/364190,nominal/mc16a/364191,nominal/mc16a/364192,nominal/mc16a/364193,nominal/mc16a/364194,nominal/mc16a/364195,nominal/mc16a/364196,nominal/mc16a/364197,nominal/mc16d/364156,nominal/mc16d/364157,nominal/mc16d/364158,nominal/mc16d/364159,nominal/mc16d/364160,nominal/mc16d/364161,nominal/mc16d/364162,nominal/mc16d/364163,nominal/mc16d/364164,nominal/mc16d/364165,nominal/mc16d/364166,nominal/mc16d/364167,nominal/mc16d/364168,nominal/mc16d/364169,nominal/mc16d/364170,nominal/mc16d/364171,nominal/mc16d/364172,nominal/mc16d/364173,nominal/mc16d/364174,nominal/mc16d/364175,nominal/mc16d/364176,nominal/mc16d/364177,nominal/mc16d/364178,nominal/mc16d/364179,nominal/mc16d/364180,nominal/mc16d/364181,nominal/mc16d/364182,nominal/mc16d/364183,nominal/mc16d/364184,nominal/mc16d/364185,nominal/mc16d/364186,nominal/mc16d/364187,nominal/mc16d/364188,nominal/mc16d/364189,nominal/mc16d/364190,nominal/mc16d/364191,nominal/mc16d/364192,nominal/mc16d/364193,nominal/mc16d/364194,nominal/mc16d/364195,nominal/mc16d/364196,nominal/mc16d/364197
  MCweight: XXX_PROMPT_MCWEIGHT

Sample: ttbar
  Type: BACKGROUND
  Title: "ttbar"
  TexTitle: $ttbar$
  FillColor: 876
  LineColor: 1
  NtupleName: dilep
  NtupleFiles: nominal/mc16a/410472,nominal/mc16d/410472
  MCweight: XXX_PROMPT_MCWEIGHT
