#!/bin/bash
#SBATCH -p htc
#SBATCH -N 1
#SBATCH --time=01:00:00
#SBATCH --ntasks-per-node=1
#SBATCH --mem=4G
#SBATCH -o log%J.txt
python test.py $@

