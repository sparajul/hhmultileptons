import ROOT ,sys
from ROOT import TMVA
from array import array
TMVA = ROOT.TMVA
reader = TMVA.Reader()
#inFile = ROOT.TFile.Open('450662.root')
inFile = ROOT.TFile.Open(sys.argv[1])
outputFile = ROOT.TFile.Open("%s_bdtg.root"%inFile.GetName().split(".")[0],"RECREATE")
thisTree = inFile.trilep
newTree  = thisTree.CloneTree(0)
#newTree.Branch('bdtg_2lOS',bdtg,'bdtg/F')

lep_Pt_0 = array('f',[0])
lep_Pt_1 = array('f',[0])
lep_Pt_2 = array('f',[0])
HT_jets = array('f',[0])
HT_lep = array('f',[0])
Mll01 = array('f',[0])
Mll02 = array('f',[0])
Mll12 = array('f',[0])
Mlll012 = array('f',[0])
DRll01 = array('f',[0])
DRll02 = array('f',[0])
DRll12 = array('f',[0])
MET_RefFinal_et = array('f',[0])
bdtg = array('f',[0])
newTree.Branch('bdtg_3l',bdtg,'bdtg/F')

reader.AddVariable('lep_Pt_0',lep_Pt_0)
reader.AddVariable('lep_Pt_1',lep_Pt_1)
reader.AddVariable('lep_Pt_2',lep_Pt_2)
reader.AddVariable("HT_jets",HT_jets)
reader.AddVariable("Mll01",Mll01)
reader.AddVariable("Mll02",Mll02)
reader.AddVariable("Mll12",Mll12)
reader.AddVariable("Mlll012",Mlll012)
reader.AddVariable('HT_lep',HT_lep)
reader.AddVariable('DRll01',DRll01)
reader.AddVariable('DRll02',DRll02)
reader.AddVariable('DRll12',DRll12)
reader.AddVariable('MET_RefFinal_et',MET_RefFinal_et)
reader.BookMVA('BDT Method','training_BDTG.weights.xml')


#thisTree = inFile.dilep
#newTree  = thisTree.CloneTree(0)
#bdtg = array('f',[0])
#newTree.Branch('bdtg_2lOS',bdtg,'bdtg/F')

for ev in thisTree:
    lep_Pt_0[0] = ev.lep_Pt_0
    lep_Pt_1[0] = ev.lep_Pt_1
    lep_Pt_2[0] = ev.lep_Pt_2
    HT_jets[0] = ev.HT_jets
    HT_lep[0] = ev.HT_lep
    Mll02[0] = ev.Mll02
    Mll01[0] = ev.Mll01
    Mll12[0] = ev.Mll12
    Mlll012[0] = ev.Mlll012
    DRll01[0] = ev.DRll01
    DRll02[0] = ev.DRll02
    DRll12[0] = ev.DRll12
    MET_RefFinal_et[0] = ev.MET_RefFinal_et
#    bdtg = array('f',[0])
 #   newTree.Branch('bdtg_2lOS',bdtg,'bdtg/F')
  #  print reader.EvaluateMVA('BDT Method')
    bdtg[0] = reader.EvaluateMVA('BDT Method')
    newTree.Fill()
newTree.Write()
#newTree.AutoSave
outputFile.Close()
    
