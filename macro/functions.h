#include <iostream>
#include <TFile.h>
#include <TLorentzVector.h>
#include <TMath.h>
#include <TLorentzVector.h>
using VecI = const ROOT::RVec<int>&;
using VecF = const ROOT::RVec<float>&;
using VecD = const ROOT::RVec<double>&;

float getDeltaRljmin(float lepEta, float lepPhi, VecF jet_eta, VecF jet_phi)
{
    float drl0jmin = 100;
    for(size_t i =0; i < jet_eta.size(); ++i)
    {
        auto j_eta = jet_eta.at(i);
	auto j_phi = jet_phi.at(i);
	auto tmp_drMin = sqrt(((lepEta - j_eta)*(lepEta - j_eta)) + ((lepPhi - j_phi)*(lepPhi - j_phi)));
	if (tmp_drMin < drl0jmin) drl0jmin = tmp_drMin;
    }
    return drl0jmin;
}

float getDeltaRLJ(float lepEta, float lepPhi, float jet_eta, float jet_phi)
{
    return sqrt( ((lepEta - jet_eta)*(lepEta - jet_eta)) + ((lepPhi - jet_phi)*(lepPhi - jet_phi) ));
}


float getDeltaRJJMax(VecF jet_eta, VecF jet_phi)
{
     float dRLJMax= 0;
     for(size_t i =0; i < jet_eta.size(); ++i)
     {
	 for(size_t j = i+1 ; j < jet_eta.size(); ++j)
	 {
             auto jet1_eta 	= jet_eta.at(i);
	     auto jet2_eta	= jet_eta.at(j);
	     auto jet1_phi 	= jet_phi.at(i);
	     auto jet2_phi	= jet_phi.at(j);
	     auto tmpDRLJMax    = sqrt( ((jet1_eta -jet2_eta)*(jet1_eta -jet2_eta)) + ((jet1_phi - jet2_phi)*(jet1_phi - jet2_phi)) );
	     if (dRLJMax < tmpDRLJMax)  dRLJMax = tmpDRLJMax;
	 }
     }
     return dRLJMax;
}

float getDeltaRJ0J1(float jet1Eta, float jet1Phi, float jet2Eta, float jet2Phi)
{
     return sqrt( ((jet1Eta - jet2Eta)*(jet1Eta - jet2Eta)) + ((jet1Phi - jet2Phi)*(jet1Phi - jet2Phi)) );
}

float getMJ0J1(float jet1Eta, float jet1Phi, float jet2Eta, float jet2Phi, float jet1E, float jet2E, float jet1Pt, float jet2Pt)
{
     TLorentzVector jet1, jet2;
          jet1.SetPtEtaPhiE (jet1Pt, jet1Eta, jet1Phi, jet1E);
          jet2.SetPtEtaPhiE (jet2Pt, jet2Eta, jet2Phi, jet2E);
        float mjj = (jet1 + jet2).M();
     
     return float (mjj);
}

float getML0J0(float lep1Eta, float lep1Phi, float jet1Eta, float jet1Phi, float lep1E, float jet1E, float lep1Pt, float jet1Pt)
{
     TLorentzVector lep1, jet1;
          lep1.SetPtEtaPhiE (lep1Pt, lep1Eta, lep1Phi, lep1E);
          jet1.SetPtEtaPhiE (jet1Pt, jet1Eta, jet1Phi, jet1E);
        float ml0j0 = (lep1 + jet1).M();

     return float (ml0j0);
}

float getML0J1(float lep1Eta, float lep1Phi, float jet2Eta, float jet2Phi, float lep1E, float jet2E, float lep1Pt, float jet2Pt)
{
     TLorentzVector lep1, jet2;
          lep1.SetPtEtaPhiE (lep1Pt, lep1Eta, lep1Phi, lep1E);
          jet2.SetPtEtaPhiE (jet2Pt, jet2Eta, jet2Phi, jet2E);
        float ml0j1 = (lep1 + jet2).M();

     return float (ml0j1);
}

float getML1J0(float lep2Eta, float lep2Phi, float jet1Eta, float jet1Phi, float lep2E, float jet1E, float lep2Pt, float jet1Pt)
{
     TLorentzVector lep2, jet1;
          lep2.SetPtEtaPhiE (lep2Pt, lep2Eta, lep2Phi, lep2E);
          jet1.SetPtEtaPhiE (jet1Pt, jet1Eta, jet1Phi, jet1E);
        float ml1j0 = (lep2 + jet1).M();

     return float (ml1j0);
}

float getML1J1(float lep2Eta, float lep2Phi, float jet2Eta, float jet2Phi, float lep2E, float jet2E, float lep2Pt, float jet2Pt)
{
     TLorentzVector lep2, jet2;
          lep2.SetPtEtaPhiE (lep2Pt, lep2Eta, lep2Phi, lep2E);
          jet2.SetPtEtaPhiE (jet2Pt, jet2Eta, jet2Phi, jet2E);
        float ml1j1 = (lep2 + jet2).M();

     return float (ml1j1);
}

float getML2J0(float lep3Eta, float lep3Phi, float jet1Eta, float jet1Phi, float lep3E, float jet1E, float lep3Pt, float jet1Pt)
{
     TLorentzVector lep3, jet1;
          lep3.SetPtEtaPhiE (lep3Pt, lep3Eta, lep3Phi, lep3E);
          jet1.SetPtEtaPhiE (jet1Pt, jet1Eta, jet1Phi, jet1E);
        float ml2j0 = (lep3 + jet1).M();

     return float (ml2j0);
}

float getML2J1(float lep3Eta, float lep3Phi, float jet2Eta, float jet2Phi, float lep3E, float jet2E, float lep3Pt, float jet2Pt)
{
     TLorentzVector lep3, jet2;
          lep3.SetPtEtaPhiE (lep3Pt, lep3Eta, lep3Phi, lep3E);
          jet2.SetPtEtaPhiE (jet2Pt, jet2Eta, jet2Phi, jet2E);
        float ml2j1 = (lep3 + jet2).M();

     return float (ml2j1);
}
