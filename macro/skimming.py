
import ROOT ,sys
  
#input file I choose mc16d only
inFile = ROOT.TFile.Open(sys.argv[1])
outputFile = ROOT.TFile.Open("%s_skim.root"%inFile.GetName().split(".")[0],"RECREATE")# inFile = 450661.root: inFile.GetName().split(".") = ['450661','root']

thisTree = inFile.dilep #inFile.Get('dilep') are same
newTree  = thisTree.CloneTree(0)
for ev in thisTree:
    if ( (ev.total_charge == 0) and (ev.lep_isLooseLH_0 or ev.lep_isLoose_0) and (ev.lep_isLooseLH_1 or ev.lep_isLoose_1) and (ev.lep_chargeIDBDTTight_0 > -0.337671) and (ev.lep_chargeIDBDTTight_1 > -0.337671) and (ev.nJets_OR_T_MV2c10_70 ==0) and (ev.nJets_OR_T >1)):
        newTree.Fill()

newTree.AutoSave()
