import ROOT as R
#R.ROOT.EnableImplicitMT()
import sys,glob

R.gInterpreter.Declare('#include <functions.h>')

files  =glob.glob(sys.argv[1]+"/*")

tdpFile= "/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/dev/AnalysisTop/TopDataPreparation/XSection-MC15-13TeV.data"
d = dict([ (line.split()[0],line.split()[1:]) for line in open(tdpFile) if len(line.split())>0 and line.split()[0] !="#"])

def getXS(dsid):
    return float(d[str(dsid)][0])*float(d[str(dsid)][1])

treeName ='nominal'
ch =  R.TChain(treeName)
smw = R.TChain("sumWeights")
for f in files:
    ch.AddFile(f)
    smw.AddFile(f)

totW=0.0
for s in smw:
    totW = totW+ s.totalEventsWeighted

df     = R.RDataFrame(ch)
smdf   = R.RDataFrame(smw)
myDf   = df.Define('DRl0jmin','getDeltaRljmin(lep_Eta_0,lep_Phi_0,jet_eta,jet_phi)')
myDf   = myDf.Define('DRl1jmin','getDeltaRljmin(lep_Eta_1,lep_Phi_1,jet_eta,jet_phi)')
myDf   = myDf.Define('DRl0j1','getDeltaRLJ(lep_Eta_0,lep_Phi_0,lead_jetEta,lead_jetPhi)')
myDf   = myDf.Define('DRl0j2','getDeltaRLJ(lep_Eta_0,lep_Phi_0,sublead_jetEta,sublead_jetPhi)')
myDf   = myDf.Define('DRl1j1','getDeltaRLJ(lep_Eta_1,lep_Phi_1,lead_jetEta,lead_jetPhi)')
myDf   = myDf.Define('DRl1j2','getDeltaRLJ(lep_Eta_1,lep_Phi_1,sublead_jetEta,sublead_jetPhi)')
myDf   = myDf.Define('DRl2j1','getDeltaRLJ(lep_Eta_2,lep_Phi_2,lead_jetEta,lead_jetPhi)')
myDf   = myDf.Define('DRl2j2','getDeltaRLJ(lep_Eta_2,lep_Phi_2,sublead_jetEta,sublead_jetPhi)')
myDf   = myDf.Define('DRjjMax','getDeltaRJJMax(jet_eta,jet_phi)')
myDf   = myDf.Define('DRj0j1','getDeltaRJ0J1(lead_jetEta,lead_jetPhi,sublead_jetEta,sublead_jetPhi)')
myDf   = myDf.Define('mj0j1','getMJ0J1(lead_jetEta,lead_jetPhi,sublead_jetEta,sublead_jetPhi,lead_jetE,sublead_jetE,lead_jetPt,sublead_jetPt)')
myDf   = myDf.Define('ml0j0','getML0J0(lep_Eta_0,lep_Phi_0,lead_jetEta,lead_jetPhi,lep_E_0,lead_jetE,lep_Pt_0,lead_jetPt)')
myDf   = myDf.Define('ml0j1','getML0J1(lep_Eta_0,lep_Phi_0,sublead_jetEta,sublead_jetPhi,lep_E_0,sublead_jetE,lep_Pt_0,sublead_jetPt)')
myDf   = myDf.Define('ml1j0','getML1J0(lep_Eta_1,lep_Phi_1,lead_jetEta,lead_jetPhi,lep_E_1,lead_jetE,lep_Pt_1,lead_jetPt)')
myDf   = myDf.Define('ml1j1','getML1J1(lep_Eta_1,lep_Phi_1,sublead_jetEta,sublead_jetPhi,lep_E_1,sublead_jetE,lep_Pt_1,sublead_jetPt)')
myDf   = myDf.Define('ml2j0','getML2J0(lep_Eta_2,lep_Phi_2,lead_jetEta,lead_jetPhi,lep_E_2,lead_jetE,lep_Pt_2,lead_jetPt)')
myDf   = myDf.Define('ml2j1','getML2J1(lep_Eta_2,lep_Phi_2,sublead_jetEta,sublead_jetPhi,lep_E_2,sublead_jetE,lep_Pt_2,sublead_jetPt)')
myDf   = myDf.Define("totalEventsWeighted",'float(TPython::Eval("totW"))')

opts   = R.RDF.RSnapshotOptions()
opts.fMode="UPDATE"

brnchList=['RunYear', 'weight_mc', 'weight_pileup', 'weight_jvt', 'mc_xSection', 'mc_kFactor', 'total_leptons', 'total_charge', 'HLT_mu20_iloose_L1MU15', 'HLT_mu50', 'HLT_mu26_ivarmedium', 'HLT_mu22_mu8noL1', 'HLT_e60_lhmedium_nod0', 'HLT_mu18_mu8noL1', 'HLT_e26_lhtight_nod0_ivarloose', 'HLT_e24_lhmedium_L1EM20VH', 'HLT_e17_lhloose_nod0_mu14', 'HLT_2e12_lhloose_L12EM10VH', 'HLT_e120_lhloose', 'HLT_2e24_lhvloose_nod0', 'HLT_e140_lhloose_nod0', 'HLT_e60_lhmedium', 'HLT_2e17_lhvloose_nod0', 'HLT_e17_lhloose_mu14', 'met_met', 'met_phi', 'loose', 'Mll01', 'Mll02', 'Mll12', 'Mlll012', 'Ptll01', 'Ptll02', 'Ptll12', 'DRll01', 'DRll02', 'DRll12', 'best_Z_Mll', 'best_Z_other_Mll', 'best_Z_other_MtLepMet', 'minOSSFMll', 'minOSMll', 'nJets_OR',  'nJets_OR_DL1_85', 'nJets_OR_DL1_77', 'nJets_OR_DL1_70', 'nJets_OR_DL1r_85', 'nJets_OR_DL1r_77', 'nJets_OR_DL1r_70', 'nTaus_OR_Pt25', 'nTaus_OR_Pt25_RNN', 'isBlinded', 'HT', 'HT_lep', 'HT_jets', 'lead_jetPt', 'lead_jetEta', 'lead_jetPhi', 'lead_jetE', 'sublead_jetPt', 'sublead_jetEta', 'sublead_jetPhi', 'sublead_jetE','onelep_type','dilep_type','trilep_type','quadlep_type','higgs1Mode','higgs2Mode','eventNumber','runNumber','mcChannelNumber','totalEventsWeighted','DRl0jmin','DRl1jmin','DRl0j1','DRl0j2','DRl1j1','DRl1j2','DRl2j1','DRl2j2','DRj0j1', 'DRjjMax', 'mj0j1','ml0j0','ml0j1','ml1j0','ml1j1','ml2j0','ml2j1']



mult_brnchList= ['lep_ID_', 'lep_Pt_', 'lep_E_', 'lep_Eta_', 'lep_Phi_', 'lep_isTightLH_', 'lep_isMediumLH_', 'lep_isLooseLH_', 'lep_isLoose_', 'lep_isTight_', 'lep_isMedium_', 'lep_isolationLoose_', 'lep_isolationGradient_', 'lep_isolationGradientLoose_', 'lep_isolationTightTrackOnly_', 'lep_isolationFCLoose_', 'lep_isolationFCTight_', 'lep_isolationPflowTight_', 'lep_isolationPflowLoose_', 'lep_isPrompt_', 'lep_truthType_', 'lep_truthOrigin_', 'lep_chargeIDBDTLoose_', 'lep_chargeIDBDTResult_','tau_pt_', 'tau_eta_', 'tau_charge_', 'tau_width_', 'tau_passJetIDRNNLoose_', 'tau_passJetIDRNNMed_', 'tau_passEleOLR_', 'tau_passEleBDT_', 'tau_passEleBDTMed_', 'tau_passMuonOLR_', 'lep_isFakeLep_', 'lep_isTrigMatch_', 'lep_isTrigMatchDLT_', 'lep_ambiguityType_']

brnch = R.vector('string')()

for entry in mult_brnchList:
    if 'tau_' in entry:
        for i in xrange(2):
            brnch.push_back(entry+str(i))
    else:
        for i in xrange(4):
            brnch.push_back(entry+str(i))
        

for entry in brnchList:
    brnch.push_back(entry)

myDf.Snapshot(treeName,sys.argv[1].split('.')[2]+".root",brnch,opts)

brnch = R.vector('string')()
brnch.push_back('dsid')
brnch.push_back('generators')
brnch.push_back('AMITag')
brnch.push_back('totalEventsWeighted')
#brnch.push_back('totalEventsWeighted_mc_generator_weights')
#brnch.push_back('names_mc_generator_weights')
brnch.push_back('totalEvents')


smdf.Snapshot('sumWeights',sys.argv[1].split('.')[2]+".root",brnch,opts)
