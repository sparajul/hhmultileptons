#!/bin/env python
import ROOT as r
import json
from Config import getSampleLists
from numpy import dot, random
from array import array

mybins=None
mybins_truth=None
bound = None
bound_truth = None
Ndiff=None
produceResmats=True
nres=1000

dorebin=True

samples,samples_MC,samples_BCKG,samples_data,ttbar = getSampleLists()

def testfiles(truth,resmat,recofortest):
    myreco = dot(truth,resmat)
    print len(myreco), len(recofortest), len(truth)
    flag=True
    assert(len(myreco)==len(recofortest)),'check resmat truth failed, not same dimension'

    for i in xrange(len(myreco)):
        #print '---------------->>>>>>>>>>>>>>>>>>>>> ', i, abs(1-myreco[i]/recofortest[i]), ' dot: ',myreco[i], ' reco: ', recofortest[i], ' truth: ',truth, ' resmat: ',resmat
        if recofortest[i]==0.:print 'recofortest  ',recofortest[i]
        if abs(1-myreco[i]/recofortest[i])>0.00005: 
            print '---------------->>>>>>>>>>>>>>>>>>>>> point failed', i, abs(1-myreco[i]/recofortest[i]), ' dot: ',myreco[i], ' reco: ', recofortest[i], ' truth: ',truth, ' resmat: ',resmat
            flag=False
    if flag: 
        print  '---------------->>>>>>>>>>>>>>>>>>>>> test succeeded'
    else:
        print  '---------------->>>>>>>>>>>>>>>>>>>>> test failed'


def histo2list(histo):
    nbinsx = histo.GetNbinsX()
    nbinsy = histo.GetNbinsY()
    binlist = [histo.GetBinContent(x,y) for y in xrange(1,nbinsy+1) for x in xrange(1,nbinsx+1)]
    return binlist

def getBckg(hfile,yieldsOnly=False):
    bckgdict = {}
    for sample in samples_BCKG+['bckg','qcdmmmuon','qcdmmele']:
        samp = sample.lower()
        histo = hfile.Get(samp)
        bins = histo2list(histo)
        bckgdict[samp] = bins
    
    #print bckgdict
    wbb = bckgdict['wjetspythiaauto_hfor_wbb']
    wcc = bckgdict['wjetspythiaauto_hfor_wcc']
    bckgdict['wjetspythiaauto_hfor_wbbcc'] = [bb+cc 
                                              for bb,cc in zip(wbb,wcc)]
    bckgdict = {key: value for key,value in bckgdict.items()
                if (key!='wjetspythiaauto_hfor_wbb' and key!='wjetspythiaauto_hfor_wcc')}
    return bckgdict

def getData(hfile,dataname,yieldsOnly=False):
    histo = hfile.Get(dataname)
    print histo,'    ',dataname

    bins = histo2list(histo)
    return bins
   
def getEff(resmat,htruth,y):
    nxx = htruth.GetNbinsX()
    xx = y%nxx
    if xx==0: xx=nxx
    yy = int((y-1)/nxx)+1
    if htruth.GetBinContent(xx,yy)==0. or resmat.Integral(0,-1,y,y)==0.: 
        #print 'x: ',xx,'  y: ',yy,'  int  ',resmat.Integral(0,-1,y,y)
        return 0.
    return resmat.Integral(0,-1,y,y)/htruth.GetBinContent(xx,yy)

def getMigration(hfile,truthfile,yieldsOnly=False,fluctuate=False):
    resmat = hfile.Get('resmat')
    resmat_clone = resmat.Clone('normalized_resmat')
    resmat_clone.Scale(1./resmat_clone.Integral(0,-1,0,-1))

    htruth = truthfile.Get('truth')

    nbinsx = resmat.GetNbinsX()
    nbinsy = resmat.GetNbinsY()
    migration = []
    migerr =[]
    mig=[]

    for y in xrange(1,nbinsy+1):
      print resmat.Integral(y,y,0,-1)
     
    for y in xrange(1,nbinsy+1):
        truthbin = []
        truthbinerr = []
        truth = []
        norm = resmat.Integral(0,-1,y,y)
        for x in xrange(1,nbinsx+1):
            value=0.
            if resmat.GetBinContent(x,y) >0:
                if fluctuate: 
                    value = (resmat.Integral()*random.poisson(resmat.GetBinContent(x,y)*resmat.GetEntries()/resmat.Integral())/resmat.GetEntries())/norm
                else: value = resmat.GetBinContent(x,y)/norm
                truthbinerr.append(resmat_clone.GetBinError(x,y))
                truth.append(resmat_clone.GetBinContent(x,y))

            value = value*getEff(resmat,htruth,y)
            truthbin.append(value)
        migration.append(truthbin)
        migerr.append(truthbinerr)
        mig.append(truth)
    return migration,mig,migerr


def writejsons(var='map_dymtt',channel='muel',sels=[],name=''):
    jsondir = 'smearedMatrices/'
    hfiles = []
    yieldsOnlys = []
    for sel in sels:
        yieldsOnlys.append('0tag' in sel)
        filename = '../BoostedAnalysis-00-00-04/data_dy0.7_M1213/histos/nominal/%(channel)s/%(var)s_%(sel)s.root'%\
            {'var':var, 'channel':channel, 'sel':sel}
        hfile = r.TFile(filename)
        hfiles.append(hfile)

    import itertools

    from utils import writefile
    recofortest = []
    datanames = ['data','reco']
    #datanames = ['data','reco','protosA6neg','protosA4neg','protosA2neg','protosA2pos','protosA4pos','protosA6pos']

    for dataname in datanames:
        data = list(itertools.chain.from_iterable(getData(hfile,dataname,yo) for hfile,yo in zip(hfiles,yieldsOnlys)))
#        writefile(jsondir+dataname+'_'+name,data)

        if 'reco' in dataname:
            bckgtmp = list(itertools.chain.from_iterable(getData(hfile,'bckg',yo) for hfile,yo in zip(hfiles,yieldsOnlys)))
            asimov = [d+b for d,b in zip(data,bckgtmp)]
#            writefile(jsondir+'asimov_'+name,asimov)
            recofortest=data

#    bckgs = [getBckg(hfile,yo) for hfile,yo in zip(hfiles,yieldsOnlys)]
#    bckg  = dict( 
#        ( k, list(itertools.chain.from_iterable(b[k] for b in bckgs)) ) 
#        for k in bckgs[0] )
#    writefile(jsondir+'bckg_'+name,bckg)
   
    #truthfile = r.TFile('data/histos/truth/%s_117050_fast.root'%var)
    #truthfile = r.TFile('../tags/Analysis-00-00-04_2/data/histos/truth/%s_117050_fast_betacut.root'%var)
    truthfile = r.TFile('../BoostedAnalysis-00-00-04/data_dy0.7_M1213/histos/truth/%s_combinedTruth_wcuts_powheg110404.root'%var) 
#    truthfile = r.TFile('data/histos/truth/%s_110404_fulltruth.root'%var)
    truth = getData(truthfile,'truth')

#    writefile(jsondir+'truth',truth)
    print 'hfiles  ',hfiles
    resmats = []
    migserr =[]
    migs =[]

    for hfile in hfiles:
        arg1,arg2,arg3 = getMigration(hfile,truthfile)
        resmats.append(arg1)
        migs.append(arg2)
        migserr.append(arg3)

    resmat  = [list(itertools.chain(*row)) for row in zip(*resmats)]
    migerr  = [list(itertools.chain(*row)) for row in zip(*migserr)]
    mig     = [list(itertools.chain(*row)) for row in zip(*migs)]

#    writefile(jsondir+'resmat_'+name,resmat)
#    writefile(jsondir+'migerr_'+name,migerr)
#    writefile(jsondir+'mig_'+name,mig)

#    resmats = [getMigration(hfile,truthfile,yo) for hfile,yo in zip(hfiles,yieldsOnlys)]
#    resmat  = [list(itertools.chain(*row)) for row in zip(*resmats)]
    testfiles(truth,resmat,recofortest)

####produces ensembles of RESMAT
    startres=0
    endres=1000
    if produceResmats:
        for ires in xrange(nres):
            if ires<startres: continue
            if ires>endres:break
            if ires%1==0:print ' mat ',ires
            resmats = []
            for hfile in hfiles:
                arg1,arg2,arg3 = getMigration(hfile,truthfile,True)
                resmats.append(arg1)

            resmat  = [list(itertools.chain(*row)) for row in zip(*resmats)]
            writefile(jsondir+'resmat_smeared%i_'%(ires)+name,resmat)

            #resmats = [getMigration(hfile,truthfile,yo,True) for hfile,yo in zip(hfiles,yieldsOnlys)]
            #resmat  = [list(itertools.chain(*row)) for row in zip(*resmats)]
            #writefile(jsondir+'resmat_smeared%i_'%(ires)+name,resmat)

####END produces ensembles of RESMAT




if(__name__=="__main__"):

    import sys
    import utils
    var,sel = utils.setvarandsel(sys.argv)
    sels = [sel]
    if 'combine12tag' in sel: 
        sels = ['1tagex','2tagin'] 
    if 'combine012tag' in sel: 
        sels = ['0tagex','1tagex','2tagin'] 
    if 'combine012tagQ' in sel: 
        sels = ['0tagexQ+','1tagexQ+','2taginQ+',
                '0tagexQ-','1tagexQ-','2taginQ-'] 
    if 'combine12tagQ' in sel: 
        sels = ['1tagexQ+','2taginQ+',
                '1tagexQ-','2taginQ-'] 
    if 'combine2tagQ' in sel: 
        sels = ['2taginTRFQ+',
                '2taginTRFQ-'] 
    print sels,'_'.join(sels)

    writejsons(var=var,sels=sels,channel='muel',name=sel)


