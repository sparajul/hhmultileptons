import ROOT, sys
name = 'training'
TMVA=ROOT.TMVA
outfile = ROOT.TFile.Open('%s_tmva.root' % name, 'RECREATE')
factory = TMVA.Factory(name, outfile, 'Classification')
dLoader = TMVA.DataLoader(name)

sigFile = ROOT.TFile.Open(sys.argv[1])
bkgFile = ROOT.TFile.Open(sys.argv[2])

dLoader.AddSignalTree(sigFile.trilep)
dLoader.AddBackgroundTree(bkgFile.trilep)

dLoader.AddVariable('lep_Pt_0', 'F')
dLoader.AddVariable('lep_Pt_1', 'F')
dLoader.AddVariable('lep_Pt_2', 'F')
dLoader.AddVariable("HT_jets",'F')
dLoader.AddVariable("Mll01",'F')
dLoader.AddVariable("Mll02",'F')
dLoader.AddVariable("Mll12",'F')
dLoader.AddVariable("Mlll012",'F')
dLoader.AddVariable('HT_lep','F')
dLoader.AddVariable('DRll01','F')
dLoader.AddVariable('DRll02','F')
dLoader.AddVariable('DRll12','F')
dLoader.AddVariable('MET_RefFinal_et','F')

#sCuts = ROOT.TCut("abs(lep_ID_0)==11|| abs(lep_ID_1)==13")
sCuts = ROOT.TCut()
bCuts = ROOT.TCut()
#dLoader.PrepareTrainingAndTestTree( sCuts, bCuts, "nTrain_Signal=50000:nTrain_Background=10000000:SplitMode=Random:NormMode=NumEvents:!V")
dLoader.PrepareTrainingAndTestTree( sCuts, bCuts,"nTrain_Signal=5000:nTrain_Background=50000:nTest_Signal=5000:nTest_Background=50000:SplitMode=Random:NormMode=NumEvents:!V")

#factory.BookMethod(dLoader,TMVA.Types.kBDT, "BDT", "MinNodeSize=0.2%:MaxDepth=3")
factory.BookMethod(dLoader,TMVA.Types.kBDT, "BDTG", "BoostType=Grad")
#factory.BookMethod(TMVA.Types.kMLP, "MLP", "HiddenLayers=4:TrainingMethod=BFGS")
factory.TrainAllMethods()
factory.TestAllMethods()
factory.EvaluateAllMethods()

outfile.Close()
TMVA.TMVAGui('%s_tmva.root' % name)
